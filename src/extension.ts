import { ExtensionContext, workspace, WorkspaceEdit, TextDocument, Range, Disposable } from 'vscode';

interface IRuleDefinition {
  find: string;
  replace: string;
}

interface IRuleDefinitions {
  [ruleName: string]: IRuleDefinition;
}

interface IRuleSets {
  [fileExt: string]: IRuleDefinitions;
}

let disposable: Disposable;

export function activate(context: ExtensionContext) {
  const configRules = workspace.getConfiguration("regexReplaceOnSave").get<IRuleSets>("ruleSets");

  if (configRules) {
    const fileExts = Object.keys(configRules);

    disposable = workspace.onWillSaveTextDocument(({ document, waitUntil }) => {
      const fileName = document.fileName;
      const fileExt = fileExts.find(ext => fileName.endsWith(ext));

      if (fileExt) {
        waitUntil(updateTextDocument(document, configRules[fileExt]));
      }
    });
  }
}

export function deactivate() {
  disposable?.dispose();
  disposable = undefined as any;
}

function updateTextDocument(document: TextDocument, ruleDefinitions: IRuleDefinitions) {
  const edit = new WorkspaceEdit();
  const rules = Object.values(ruleDefinitions);

  const newText = rules.reduce((text, rule) => {
    return text.replace(new RegExp(rule.find, 'g'), rule.replace);
  }, document.getText());

  edit.replace(
    document.uri,
    new Range(0, 0, document.lineCount, 0),
    newText
  );

  return workspace.applyEdit(edit);
}
