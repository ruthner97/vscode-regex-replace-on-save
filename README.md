# Regex Replace on Save
## Automatically run RegExp replace text before saving a file.

The motivation for this extension was to create auto formatting rules for Angular HTML templates in my projects. It could be used for many other purposes. PRs are welcome.

## Options

- File Extension to apply replace rules to
- Find rule (regExp)
- Replace rule

### Example Configuration (settings.json):

```json
  {
    ...

    "regexReplaceOnSave.ruleSets": {
    "html": {
      "Angular template interpolation padding start": {
        "find": "{{([^\\s])(.*)}}",
        "replace": "{{ $1$2}}"
      },
      "Angular template interpolation padding end": {
        "find": "{{(.*)([^\\s])}}",
        "replace": "{{$1$2 }}"
      },
      "Angular template pipe padding start": {
        "find": "([^\\s|^\\|])\\|",
        "replace": "$1 |"
      },
      "Angular template pipe padding end": {
        "find": "\\|([^\\s|^\\|])",
        "replace": "| $1"
      }
    }
  }
```
## Under the hood

This function is called on each file with a matching file extension right before save.

``` typescript
  ...

  const newText = rules.reduce((text, rule) => {
    return text.replace(new RegExp(rule.find, 'g'), rule.replace);
  }, document.getText());
```
